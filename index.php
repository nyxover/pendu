<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Pendu</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">
          <img alt="Pendu" src="...">
        </a>
      </div>
    </div>
  </nav>
  <div class="container">
<div class="col-md-4 col-md-offset-4">

<h1>Le pendu de la mort</h1>

<?php
require_once 'fonctions.inc.php';

session_start();
if(isset($_POST['quitter'])) session_unset();
if(isset($_POST['rejouer'])) unset($_SESSION['mot']) ;

if (isset ($_POST['niveau']) && isset ($_POST['pseudo']))
{
if($_POST['niveau']>0 && $_POST['niveau']<4 && strlen($_POST['pseudo']) != 0)
{
$_SESSION['niveau']=(int)$_POST['niveau'] ;
$_SESSION['pseudo']=$_POST['pseudo'] ;
}
}

if(!isset($_SESSION['pseudo']))
{//1er formulaire : demande du pseudo et du niveau de jeu
$Html='
<form action="index.php" method="post">
<p><input type="radio" name="niveau" value ="1"/> facile
<input type="radio" name="niveau" value ="2"/> difficile
<input type="radio" name="niveau" value ="3"/> expert</p>
<p>Entre ton prénom (ou ton pseudo) :
<input type="text" name="pseudo"/> </p>
<p><input type="submit" value="Envoyer"/></p>
</form>';
}
else
{//Le pseudo et le niveau de jeu sont connus
if(!isset($_SESSION['mot']))
{
$Mot=MotATrouver($_SESSION['niveau']) ;
$_SESSION['mot']=$Mot;
$Longueur=strlen($Mot) ;
$_SESSION['longueur']=$Longueur ;
$_SESSION['liste']="" ;
$Reponse=str_repeat('-',$Longueur) ;
$Reponse[0]=$Mot[0] ;
$Reponse[$Longueur-1]=$Mot[$Longueur-1] ;
$_SESSION['reponse']=$Reponse ;

$_SESSION['coups_restants']=Init_Coups_Restants($_SESSION['niveau']) ;
}
elseif(isset($_POST['lettre']))
{
$lettre=strtoupper($_POST['lettre']) ;
if(Recherche_Chaine_Dans_Tableau($_SESSION['liste'],$lettre)===FALSE)
{
if(Actualise_Reponse($lettre)==0) $_SESSION['coups_restants']-- ;
}
}

if($_SESSION['coups_restants']==0)
{
$Html='
<p>'.$_SESSION['pseudo'].', perdu, tu es pendu !</p>
<p>C\'était le mot '.$_SESSION['mot'].'</p>
<p><img src="./images/pendu11.png"/></p>
<form action="index.php" method="post">
<p>
<input type="submit" name="rejouer" value="Rejouer"/>
<input type="submit" name="quitter" value="Quitter">
</p>
</form>' ;
}
elseif($_SESSION['mot']!=$_SESSION['reponse'])
{
$Image=sprintf('./images/pendu%02d.png',11-$_SESSION['coups_restants']) ;
$Html='
<p>'.$_SESSION['pseudo'].', il te reste '.$_SESSION['coups_restants'].' coups avant d\'être pendu !</p>
<p>Liste : '.Assemble_Tableau_Chaine($_SESSION['liste']).'</p>
<p>Mot à trouver :'.str_replace('-',' - ',$_SESSION['reponse']).'</p>
<form action="index.php" method="post">
<p>Propose une lettre :
<input type="text" name="lettre"/>
<input type="submit" value="Envoyer">
</p>
<p>
<input type="submit" name="quitter" value="Abandonner / Quitter">
</p>
</form>
<p><center><img style="width: 300px;" src="'.$Image.'"/></center></p>' ;
}
else
{
$Html='
<p>'.$_SESSION['pseudo'].', bravo, c\'est gagné !</p>
<p>C\'était le mot '.$_SESSION['mot'].'</p>
<form action="index.php" method="post">
<p>
<input type="submit" name="rejouer" value="Rejouer"/>
<input type="submit" name="quitter" value="Quitter">
</p>
</form>' ;
}

}
echo $Html ;
?>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</div>

</body>
</html>
